# %% Autoreload for when running in a notebook
#%load_ext autoreload
#%autoreload 2

# %% Important packages
from kkcoffea import samples
import kkplot

import coffea.hist

import numpy as np
import matplotlib.pyplot as plt

from lowmu import processors

# %% Samples
sm=samples.SampleManager('samples.yaml', '/data/kkrizka/lowmu/')
fileset = sm.fileset()

# %%
out = coffea.processor.run_uproot_job(
    fileset,
    treename="SquirrelPlots/Ntuples/SquirrelPlots_NtuplesTruthToReco",
    processor_instance=processors.MyProcessor(),
    executor=coffea.processor.futures_executor,
    executor_args={'workers':4},
)

# %%
coffea.hist.plot1d(out['reso_pt'], overlay='dataset')
#plt.ylim(0,300)
plt.show()
plt.savefig('reso_pt.png')
plt.clf()

# %%
hist=out['truth_pt_vs_eta']
for d in hist.identifiers('dataset'):
    myhist=hist[d].sum('dataset','eta')
    values=myhist.values()[()]

    den=values.sum(axis=0)
    num=values[1,:]
    
    kkplot.efficiency(myhist.axis('pt').edges(), den, num, label=d)
plt.xlim(0,1)
plt.ylim(0,1)
plt.xlabel('truth particle $p_T$ [GeV]')
plt.ylabel('efficiency')
kkplot.xticks(0.1,0.02)
kkplot.yticks(0.1,0.02)

# %%
hist=out['truth_pt_vs_eta']
for d in hist.identifiers('dataset'):
    myhist=hist[d].sum('dataset')
    values=myhist.values()[()]

    edges_eta=myhist.axis('eta').edges()
    e0=np.where(edges_eta==0)[0][0]

    valp=values[:,:,e0:].sum(axis=2)
    denp=valp.sum(axis=0)
    nump=valp[1,:]

    valm=values[:,:,:e0].sum(axis=2)
    denm=valm.sum(axis=0)
    numm=valm[1,:]

    fig, ax=kkplot.subplot_ratio()
    
    mx,meff,mxerr,myerr = \
        kkplot.efficiency(myhist.axis('pt').edges(), denm, numm, label='|$eta$|<0',ax=ax[0])
    px,peff,pxerr,pyerr = \
        kkplot.efficiency(myhist.axis('pt').edges(), denp, nump, label='|$eta$|>0',ax=ax[0])

    ax[1].errorbar(mx, peff-meff, xerr=pxerr, yerr=pyerr, fmt='.')

    ax[0].set_xlim(0,1)
    ax[1].set_xlim(0,1)
    ax[0].set_ylim(0,1)
    ax[1].set_ylim(-0.05,0.05)
    ax[1].set_xlabel('truth particle $p_T$ [GeV]')
    ax[0].set_ylabel('efficiency')
    kkplot.xticks(0.1,0.02,ax=ax[0])
    kkplot.xticks(0.1,0.02,ax=ax[1])
    kkplot.yticks(0.1,0.02,ax=ax[0])
    kkplot.yticks(0.05,0.01,ax=ax[1])
    fig.savefig('eff_etaasym_pt.png')
    fig.show()
