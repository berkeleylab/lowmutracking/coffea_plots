import coffea.processor

import awkward as ak
import numpy as np

class MyProcessor(coffea.processor.ProcessorABC):
    def __init__(self):
        super().__init__()

        self.accumulator = coffea.processor.dict_accumulator({
            "reso_pt" : coffea.hist.Hist(
                "a.u.",
                coffea.hist.Cat("dataset", "Dataset"),
                coffea.hist.Bin("reso_pt", "(track - truth) / truth $p_T$", 100,-1,1),
                ),
            "truth_pt_vs_eta" : coffea.hist.Hist(
                "a.u.",
                coffea.hist.Cat("dataset", "Dataset"),
                coffea.hist.Bin("hasTrack", "matched to track",2,-0.5,1.5),
                coffea.hist.Bin("pt" , "truth $p_T$" , 1000,   0,  10),
                coffea.hist.Bin("eta", "truth $\eta$", 100,-2.5,2.5),
                )
        })

    def process(self, events):
        output = self.accumulator.identity()
        dataset = events.metadata['dataset']

        # quality definitions
        hastruth=(events.hasTruth==1)&(events.passedTruthSelection==1)
        goodmatch=(events.hasTrack==1)&(events.track_truthMatchProb>0.5)

        # resolution
        goodtracks=events[(events.hasTrack==1)&(events.hasTruth==1)]

        output['reso_pt'].fill(dataset=dataset, reso_pt=(goodtracks.track_pt-goodtracks.truth_pt)/goodtracks.truth_pt)

        # truth (efficiencies)
        truths=events[hastruth]
        goodmatch=(truths.hasTrack==1)&(truths.track_truthMatchProb>0.5)
        output['truth_pt_vs_eta'].fill(dataset=dataset, hasTrack=goodmatch, pt=truths.truth_pt/1e3, eta=truths.truth_eta)

        return output

    def postprocess(self, accumulator):
        return accumulator